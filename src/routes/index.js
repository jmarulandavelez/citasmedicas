const { Router } = require('express');
const router = Router();

router.get('/', ( req, res ) => {
    res.send('index endpoint');
})

router.use( require('./users') );

module.exports = router