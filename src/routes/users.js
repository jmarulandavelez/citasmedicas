const { Router } = require('express');
const nodemailer = require('nodemailer');
const router = Router();
const userController = require('../controllers/users.controller');

router.get('/users', userController.getUsers );

router.post('/users', userController.addUser );

router.put('/users/:id', userController.updateUsers );

router.delete('/users/:id' , userController.deleteUsers );

router.post('/users/send-email', async (req,res) => {
   
   const user=req.body;

   contentHTML = `
        <h1>Resumen de la cita</h1>
        <ul>
            <li>Nombre paciente: ${user.name}</li>
            <li>Email: ${user.email}</li>
        </ul>
        <p>${user.message}</p>

   `;
   
   try{
	   const transporter = nodemailer.createTransport({
		   host: 'smtp.gmail.com',
		   port: 587,
		   secureConnection: false,
		   auth: {
			   user: 'itc.noreply.test@gmail.com',
			   pass: 'Intelecto2019'
		   },
			tls:{
				ciphers:'SSLv3'
			}
	   });

	   const info = await transporter.sendMail({
		from:"'Servidor'", 
		to: user.email,
		subject: 'Informacion de la cita',
		html: contentHTML

		});
		
		let response={
			Resp:true,
			message:"Email Send"
		}
		
		res.send(response)
	}
    catch(e){
        res.status(500).json({
			Resp:false,
            message : e
        });
    }
});

module.exports = router