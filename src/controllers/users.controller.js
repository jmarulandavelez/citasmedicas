const User = require('../models/Users');

const addUser = async ( req, res ) => {
    try {
        console.log(req.body);
        const user = new User({
            name : req.body.name,
            email : req.body.email,
            password : req.body.password
        });
        user.password = await user.encryptPassword( user.password );
        await user.save();
        res.status(200).json({
            message : 'user was added successfully',
            user : user
        })
    }
    catch(e){
        res.status(500).json({
            message : e
        });
    }
}

const getUsers = async ( req, res ) => {
    try {
        const users = await User.find().sort({name : 'asc'});
        res.status(200).json({
            users : users
        })
    }
    catch(e){
        res.status(500).json({
            message : e
        });
    }
}

const findUser = async ( req, res ) => {
    try {
        const user = await User.findById( req.params.id );
        let message = '';
        let status = 200;
        if (user){
            message = '';
        }
        else {
            status = 404;
            message = 'the user do not exists'
        }
        res.status(status).json({
            message : message,
            user : user
        })
    }
    catch(e){
        res.status(500).json({
            message : e
        });
    }
}

const updateUsers = async ( req, res ) => {
    try {
        const user = await User.findByIdAndUpdate( req.params.id, req.body );
        let message = '';
        let status = 200;
        if (user){
            message = 'user was updated successfully';
        }
        else {
            status = 404;
            message = 'the user do not exists'
        }
        res.status(status).json({
            message : message,
            user : user
        })
    }
    catch(e){
        res.status(500).json({
            message : e
        });
    }
}

const deleteUsers = async ( req, res ) => {
    try {
        const user = await User.findByIdAndDelete( req.params.id );
        let message = '';
        let status = 200;
        if (user){
            message = 'user was deleted successfully';
        }
        else {
            status = 404;
            message = 'the user do not exists'
        }
        res.status(status).json({
            message : message,
            user : user
        })
    }
    catch(e){
        res.status(500).json({
            message : e
        });
    }
}

module.exports = {
    addUser,
    getUsers,
    updateUsers,
    deleteUsers
}