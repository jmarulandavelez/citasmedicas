const express = require('express');

// Init
const app = express();
const morgan = require('morgan');
require('./config/database');

// Settings
app.set( 'port', process.env.PORT || 3000 );


// Middlewares
app.use( express.json() );
app.use( express.urlencoded({ extended: false }) );
app.use(morgan('dev'));

// Routes
app.use( require('./routes/index') );

module.exports = app

